import React, { Component } from 'react';
import './assets/css/main.scss';

import TopLanding from './components/TopLanding';
import WhatWeDo from './components/WhatWeDo';
import Portfolio from './components/Portfolio';
import Contact from './components/Contact';
import Technology from './components/Technology';

import Navbar from './components/layout/Navbar';
import Footer from './components/layout/Footer';

class App extends Component {
  state = {
    scrollY: 0,
    scrollX: 0
  };

  handleScrollY = () => {
    this.setState({
      scrollY: window.scrollY
    });
  };

  componentDidMount() {
    window.addEventListener('scroll', this.handleScrollY);
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleScrollY);
  }

  render() {
    return (
      <React.Fragment>
        <Navbar positionFromTop={this.state.scrollY} />
        <TopLanding />
        <WhatWeDo />
        <Portfolio />
        <Technology />
        <Contact />
        <Footer />
      </React.Fragment>
    );
  }
}

export default App;
