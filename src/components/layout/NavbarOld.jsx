import React, { Component } from 'react';
import classnames from 'classnames';
// import { NavHashLink as NavLink } from 'react-router-hash-link';
import logo from '../../assets/images/logo.svg';
import AnchorLink from 'react-anchor-link-smooth-scroll';
import navButton from '../../assets/images/nav-button.svg';

class Navbar extends Component {
  state = {
    menuOpened: false
  };

  onClickToggleMenu = () => {
    this.setState({
      menuOpened: !this.state.menuOpened
    });
  };

  render() {
    return (
      <nav
        className={classnames({
          'fix-nav': this.props.positionFromTop > 10
        })}
      >
        <div className="tablet-only tablet-logo">
          <img src={logo} alt="" />
          <div
            className={classnames('open-menu-button mobile-only', {
              'menu-opened': this.state.menuOpened
            })}
            onClick={this.onClickToggleMenu}
          >
            <img src={navButton} alt="" />
          </div>
        </div>
        <ul
          className={classnames('flex-parent-wrap', {
            'menu-opened': this.state.menuOpened
          })}
        >
          <li>
            <AnchorLink onClick={this.onClickToggleMenu} href="#about">
              About
            </AnchorLink>
          </li>
          <li>
            <AnchorLink onClick={this.onClickToggleMenu} href="#portfolio">
              Portfolios
            </AnchorLink>
          </li>
          <li>
            <AnchorLink onClick={this.onClickToggleMenu} href="#technology">
              Technologies
            </AnchorLink>
          </li>
          <li>
            <AnchorLink onClick={this.onClickToggleMenu} href="#contact">
              Contact
            </AnchorLink>
          </li>
          <li className="nav-logo">
            {/* eslint-disable-next-line */}
            <AnchorLink href="#top-landing">
              <img src={logo} alt="" />
            </AnchorLink>
          </li>
        </ul>
      </nav>
    );
  }
}

export default Navbar;
