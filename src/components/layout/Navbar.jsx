import React, { Component } from 'react';
import classnames from 'classnames';
import logo from '../../assets/images/logo-no-text.svg';
import navButton from '../../assets/images/nav-button.svg';
import { Link as AnchorLink } from 'react-scroll';

class Navbar extends Component {
  state = {
    menuOpened: false
  };

  // componentDidMount() {
  //   Events.scrollEvent.register('begin');
  //   Events.scrollEvent.register('end');
  //   scrollSpy.update();
  // }

  // componentWillUnmount() {
  //   Events.scrollEvent.remove('begin');
  //   Events.scrollEvent.remove('end');
  // }

  onClickToggleMenu = () => {
    this.setState({
      menuOpened: !this.state.menuOpened
    });
  };

  render() {
    return (
      <nav
        className={classnames({
          'fix-nav': this.props.positionFromTop > 10
        })}
      >
        <div className="tablet-only tablet-logo">
          <img src={logo} alt="" />
          <div
            className={classnames('open-menu-button mobile-only', {
              'menu-opened': this.state.menuOpened
            })}
            onClick={this.onClickToggleMenu}
          >
            <img src={navButton} alt="" />
          </div>
        </div>
        <ul
          className={classnames('flex-parent-wrap', {
            'menu-opened': this.state.menuOpened
          })}
        >
          <li>
            <AnchorLink
              activeClass="active"
              to="about"
              spy={true}
              smooth={true}
              duration={500}
              onClick={this.onClickToggleMenu}
            >
              About
            </AnchorLink>
          </li>
          <li>
            <AnchorLink
              activeClass="active"
              to="portfolio"
              spy={true}
              smooth={true}
              duration={500}
              onClick={this.onClickToggleMenu}
            >
              Portfolios
            </AnchorLink>
          </li>
          <li>
            <AnchorLink
              activeClass="active"
              to="technology"
              spy={true}
              smooth={true}
              duration={500}
              onClick={this.onClickToggleMenu}
            >
              Technologies
            </AnchorLink>
          </li>
          <li>
            <AnchorLink
              activeClass="active"
              to="contact"
              spy={true}
              smooth={true}
              duration={500}
              onClick={this.onClickToggleMenu}
            >
              Contact
            </AnchorLink>
          </li>
          <li className="nav-logo">
            {/* eslint-disable-next-line */}
            <AnchorLink
              activeClass="active"
              to="top-landing"
              spy={true}
              smooth={true}
              duration={500}
              onClick={this.onClickToggleMenu}
            >
              <img src={logo} alt="" />
            </AnchorLink>
          </li>
        </ul>
      </nav>
    );
  }
}

export default Navbar;
