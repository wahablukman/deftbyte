import React, { Component } from 'react';
// import logo from '../assets/images/logo.svg';
import triangleSeparator from '../assets/images/triangle-lb.svg';
import { Link as AnchorLink, Events, scrollSpy } from 'react-scroll';
import Lottie from 'react-lottie';
import Parallax from 'react-rellax';

class TopLanding extends Component {
  state = {};

  componentDidMount() {
    Events.scrollEvent.register('begin');
    Events.scrollEvent.register('end');
    scrollSpy.update();
  }

  componentWillUnmount() {
    Events.scrollEvent.remove('begin');
    Events.scrollEvent.remove('end');
  }

  render() {
    const animatedLogo = {
      loop: true,
      autoplay: true,
      animationData: require('../assets/images/deftbyte-logo.json')
    };
    return (
      <section className="top-landing flex-parent-wrap vh-fix" id="top-landing">
        <img src={triangleSeparator} className="separator" alt="Separator" />
        <div className="text">
          <h1 className="animated fadeInLeft">
            <span>We assist your</span> <br />{' '}
            <span>
              <span className="go-blue">tech idea</span> grows
            </span>
            <br />
            <span>
              into&nbsp;<span className="go-blue">reality</span>
            </span>
          </h1>
          <Parallax speed={1} className="button-wrapper-flex">
            <AnchorLink
              to="about"
              spy={true}
              smooth={true}
              duration={500}
              className="btn secondary"
            >
              How ? Do tell me more!
            </AnchorLink>
          </Parallax>
        </div>
        <div className="logo">
          {/* <img src={logo} alt="logo" /> */}
          <Parallax as="span" speed={-2}>
            <Lottie options={animatedLogo} width={'60%'} height={'auto'} />
          </Parallax>
        </div>
      </section>
    );
  }
}

export default TopLanding;
