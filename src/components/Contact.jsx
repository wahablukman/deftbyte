import React, { Component } from 'react';
import axios from 'axios';
import keys from '../config/keys';
// import loadingIcon from '../assets/images/loading.svg';
import Lottie from 'react-lottie';
import { CSSTransition, TransitionGroup } from 'react-transition-group';

class Contact extends Component {
  state = {
    _subject: '',
    _honey: '',
    _replyto: '',
    name: '',
    email: '',
    message: '',
    successMessage: '',
    errorMessage: '',
    loading: false
  };

  onChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  onSubmit = async e => {
    e.preventDefault();
    await this.setState({
      loading: true
    });
    let enquiryData = new FormData();
    enquiryData.append('_subject', `${this.state.name} sent an enquiry`);
    enquiryData.append('_replyto', this.state.email);
    enquiryData.append('name', this.state.name);
    enquiryData.append('email', this.state.email);
    enquiryData.append('message', this.state.message);

    if (this.state._honey !== '') {
      return this.setState(
        {
          errorMessage: 'Form not submitted, possible SPAM',
          loading: false
        },
        () => {
          setTimeout(() => {
            this.setState({
              errorMessage: ''
            });
          }, 5000);
        }
      );
    } else {
      try {
        const submittedForm = await axios.post(
          `https://formsapi.jabwn.com/key/${keys.DeftByteJABWNAPIKey}`,
          enquiryData
        );
        if (submittedForm.data.message === 'success') {
          this.setState(
            {
              loading: false,
              successMessage: `Enquiry submitted! We'll get back to you as soon as possible`,
              name: '',
              email: '',
              message: ''
            },
            () => {
              setTimeout(() => {
                this.setState({
                  successMessage: ''
                });
              }, 5000);
            }
          );
        } else {
          this.setState(
            {
              loading: false,
              errorMessage:
                'There is an error in sending your enquiry, try again in a bit'
            },
            () => {
              setTimeout(() => {
                this.setState({
                  errorMessage: ''
                });
              }, 5000);
            }
          );
        }
      } catch (err) {
        return this.setState(
          {
            loading: false,
            errorMessage: err
          },
          () => {
            setTimeout(() => {
              this.setState({
                errorMessage: ''
              });
            }, 5000);
          }
        );
      }
    }
  };

  render() {
    const animatedLoading = {
      loop: true,
      autoplay: true,
      animationData: require('../assets/images/loading-spinner.json'),
      rendererSettings: {
        className: 'loading-animation'
      }
    };
    return (
      <React.Fragment>
        <section className="contact-section" id="contact">
          <div className="container">
            <h1>Contact Us</h1>
            <h3>
              Now that you've seen all about us, let's not waste time and
              discuss your project with us! We're <i>aaalways</i> excited for a
              new project.
            </h3>
            <form onSubmit={this.onSubmit}>
              <input
                type="text"
                name="_honey"
                onChange={this.onChange}
                style={{ display: 'none' }}
                value={this.state.honey}
              />
              <div className="form-group">
                <label>Name</label>
                <input
                  onChange={this.onChange}
                  value={this.state.name}
                  type="text"
                  name="name"
                />
              </div>
              <div className="form-group">
                <label>E-mail</label>
                <input
                  onChange={this.onChange}
                  value={this.state.email}
                  type="email"
                  name="email"
                />
              </div>
              <div className="form-group">
                <label>Enquiry</label>
                <textarea
                  onChange={this.onChange}
                  value={this.state.message}
                  rows="10"
                  name="message"
                />
              </div>
              <br />
              <button type="submit">Send enquiry</button>
            </form>
          </div>
        </section>
        <TransitionGroup>
          {this.state.loading ? (
            <CSSTransition
              key="loading"
              in={true}
              appear={true}
              classNames={{
                enter: 'animated',
                enterActive: 'fadeIn',
                exit: 'animated',
                exitActive: 'fadeOut'
              }}
              timeout={500}
            >
              <div className="loading-spinner">
                <Lottie
                  options={animatedLoading}
                  resizeMode="cover"
                  width={'25%'}
                  height={'auto'}
                />
              </div>
            </CSSTransition>
          ) : null}
          {this.state.successMessage ? (
            <CSSTransition
              key="successMessage"
              in={true}
              appear={true}
              classNames={{
                enter: 'animated',
                enterActive: 'flipInX',
                exit: 'animated',
                exitActive: 'flipOutX'
              }}
              timeout={500}
            >
              <div className="success-message">
                <div className="success-text d-inline-block">
                  {this.state.successMessage}
                </div>
              </div>
            </CSSTransition>
          ) : null}
          {this.state.errorMessage ? (
            <CSSTransition
              key="errorMessage"
              in={true}
              appear={true}
              classNames={{
                enter: 'animated',
                enterActive: 'flipInX',
                exit: 'animated',
                exitActive: 'flipOutX'
              }}
              timeout={500}
            >
              <div className="error-message">
                <div className="error-text d-inline-block">
                  {this.state.errorMessage}
                </div>
              </div>
            </CSSTransition>
          ) : null}
        </TransitionGroup>
      </React.Fragment>
    );
  }
}

export default Contact;
