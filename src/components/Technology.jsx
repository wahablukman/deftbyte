import React, { Component } from 'react';

import nodeLogo from '../assets/images/stacks/nodejs.svg';
import reactLogo from '../assets/images/stacks/react.svg';
import reduxLogo from '../assets/images/stacks/redux.svg';
import vueLogo from '../assets/images/stacks/vue.svg';
import sassLogo from '../assets/images/stacks/sass.svg';
import expressLogo from '../assets/images/stacks/express.svg';
import laravelLogo from '../assets/images/stacks/laravel.svg';
import bigcommerceLogo from '../assets/images/stacks/bigcommerce.svg';
import wordpressLogo from '../assets/images/stacks/wordpress.svg';
import mailchimpLogo from '../assets/images/stacks/mailchimp.svg';
import campaignmonitorLogo from '../assets/images/stacks/campaignmonitor.svg';

import hugeBgLogo from '../assets/images/huge-light-bg.svg';

class Technology extends Component {
  state = {};
  render() {
    return (
      <section className="technology-section" id="technology">
        <div className="huge-light-bg">
          <img src={hugeBgLogo} alt="" />
        </div>
        <div className="container">
          {/* <h1>Technology</h1> */}
          <h3 className="text-center">
            There are a lot of technologies stacks out there in order to develop
            your web / mobile / desktop application. We, DeftByte, will make the
            application based on what we think is best for our client's needs to
            meet the requirement. Here are the technologies that we have used
            but are not limited to:
          </h3>
          <div className="flex-parent-wrap all-center technologies">
            <div className=" technology">
              <img src={nodeLogo} alt="" />
            </div>
            <div className=" technology">
              <img src={reactLogo} alt="" />
            </div>
            <div className=" technology">
              <img src={reduxLogo} alt="" />
            </div>
            <div className=" technology">
              <img src={vueLogo} alt="" />
            </div>
            <div className=" technology">
              <img src={expressLogo} alt="" />
            </div>
            <div className=" technology">
              <img src={sassLogo} alt="" />
            </div>
            <div className=" technology">
              <img src={laravelLogo} alt="" />
            </div>
            <div className=" technology">
              <img src={bigcommerceLogo} alt="" />
            </div>
            <div className=" technology">
              <img src={wordpressLogo} alt="" />
            </div>
            <div className=" technology">
              <img src={mailchimpLogo} alt="" />
            </div>
            <div className=" technology">
              <img src={campaignmonitorLogo} alt="" />
            </div>
          </div>
        </div>
      </section>
    );
  }
}

export default Technology;
