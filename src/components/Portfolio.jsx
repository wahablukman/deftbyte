import React, { Component } from 'react';

import { CSSTransition, TransitionGroup } from 'react-transition-group';

import botler from '../assets/images/portfolios/botler.jpg';
import getCraft from '../assets/images/portfolios/GetCraftDesktop.jpg';
import doa from '../assets/images/portfolios/dinamikaoscaragung.png';
import kembaraNusa from '../assets/images/portfolios/KembaraNusaDesktop.jpg';
import artistikSL from '../assets/images/portfolios/ASLDesktop.jpg';
import gignest from '../assets/images/portfolios/GignestDesktop.jpg';
import wlPortfolio from '../assets/images/portfolios/WLPDesktop.jpg';
import sartorialBay from '../assets/images/portfolios/SartorialBayDesktop.jpg';
import tunaKarya from '../assets/images/portfolios/TunakaryaDesktop.jpg';
import l3bUnpad from '../assets/images/portfolios/l3bDesktop.jpg';
import zinkAndSons from '../assets/images/portfolios/ZinkAndSonsDesktop.jpg';
import newtownAS from '../assets/images/portfolios/NasDesktop.jpg';
// import kadmium from '../assets/images/portfolios/KadmiumDesktop.jpg';

class Portfolio extends Component {
  state = {
    name: '',
    image: '',
    description: '',
    technologies: '',
    year: '',
    modalOpened: false,
    portfolios: [
      {
        clientName: 'Botler',
        description:
          'Botler is a chatbot that will help you arranging schedules. He will remind you and also tell you the time required to go to your destination. When you press botler notification, it will show the best route to take in google maps.',
        year: '2018',
        technologyUsed: [
          'React Native',
          'Redux',
          'Express.js',
          'MongoDB',
          'Mocha & Chai (TDD)',
          'Travis-CI'
        ],
        image: botler,
        URL: ''
      },
      {
        clientName: 'Dinamika Oscar Agung',
        description:
          'PT. Dinamika Oscar Agung was in 2006 to provide solution for companies who demands well known engineering parts from Japan. With the principle of high level service, the company aims to satisfy its customer from the first sales process after sales process as a one stop solution for its clients.',
        year: '2018',
        technologyUsed: ['React.js', 'Node.js'],
        image: doa,
        URL: ''
      },
      {
        clientName: 'GetCraft',
        description:
          'A curated marketplace that helps brands to connect to GetCraft’s network of creators across Journalism, Design, Photography, Video and Sponsored Channels such as Publishers (Digital online publishers) Influencers (Social Media, Celebrities, Experts). Projects are managed end-to-end through GetCraft’s platform and backed up by in-house production/editorial experts to help optimise performance.',
        year: '2018',
        technologyUsed: ['React.js', 'Redux'],
        image: getCraft,
        URL: ''
      },
      {
        clientName: 'Kembara Nusa',
        description:
          'Kembara Nusa adalah yayasan yang mengajak para dokter gigi dan teman-teman relawan lain dari berbagai latar belakang pendidikan yang ingin membantu untuk meningkatkan kesehatan gigi dan mulut di wilayah Indonesia, sekaligus mengenalkan kekayaan alam Indonesia di daerah yang kami pilih sebagai project kami.',
        year: '2018',
        technologyUsed: ['Vue.js', 'Node.js'],
        image: kembaraNusa,
        URL: ''
      },
      {
        clientName: 'Artistik Salindia Lima',
        description:
          'Artistik Salindia Lima is specialised in branding, design and marketing. Their services include graphic design, corporate identity, brand guidelines, packaging, print design and media',
        year: '2018',
        technologyUsed: [
          'Vue.js',
          'Express.js',
          'Bootstrap',
          'Node.js',
          'MongoDB'
        ],
        image: artistikSL,
        URL: ''
      },
      {
        clientName: 'Gignest',
        description:
          'Gignest is an Indonesian talent showcase, providing people/ event organizer with selections of talents in the entertainment industry, including musician, stand-up comedian, magician, actor, sound engineer, and other relevant performers.',
        year: '2018',
        technologyUsed: ['Laravel', 'PHP', 'mySQL', 'SendGrid', 'Bootstrap'],
        image: gignest,
        URL: ''
      },
      {
        clientName: "Wahab Lukman's portfolio",
        description:
          "Latest updates on Wahab Lukman's activities involving around website and photography projects !",
        year: '2018',
        technologyUsed: ['Vue', 'Axios'],
        image: wlPortfolio,
        URL: ''
      },
      {
        clientName: 'Sartorial Bay',
        description:
          'Nowadays in Sydney, Massimo (the founder), continues his tailoring legacy through Sartorial Bay by offering his expertise to transform Australian men’s image. Massimo’s goal with Sartorial Bay is to blend heritage tailoring with the coastal lifestyle, to create a unique new style that embraces formal and casual wearing in an organic symbiosis.',
        year: '2017',
        technologyUsed: ['Wordpress'],
        image: sartorialBay,
        URL: ''
      },
      {
        clientName: 'TunaKarya',
        description:
          'TunaKarya adalah suatu komunitas berbasis forum yang menjadikan wadah bagi para “tunakarya” untuk berdiskusi, bercerita, memberikan info apapun yang berhubungan dengan tujuan utama mereka yaitu untuk mendapatkan pekerjaan yang layak.',
        year: '2017',
        technologyUsed: ['Wordpress', 'mySQL', 'phpBB', 'Bootstrap'],
        image: tunaKarya,
        URL: ''
      },
      {
        clientName: 'L3BUNPAD',
        description:
          'Lomba Lintas Lembah dan Bukit (L3B) merupakan acara rutin Fakultas Pertanian Universitas Padjadjaran yang pertama kali dilaksanakan tahun 1962 di kampus Unpad Dago dengan memanfaatkan lembah serta bukit di sekitarnya, diketuai oleh mahasiswa angkatan kedua di Fakultas Pertanian Universitas Padjadjaran (Faperta angkatan 1960) yaitu Ir. Sugiat dengan Kang Djungdjung Hickman (Faperta Unpad 1959) sebagai penanggung jawab dari Seksi Olah Raga Senat KMFP.',
        year: '2017',
        technologyUsed: ['HTML5', 'Vanilla JS', 'Bootstrap'],
        image: l3bUnpad,
        URL: ''
      },
      {
        clientName: 'Zink And Sons',
        description:
          'Zink and Sons has been dressing the gentlemen of Sydney since 1895 and have a reputation second to none, with Zink and Sons’ establishment now in its 6th generation under the ownership of Robert Jones and his son Daniel.',
        year: '2016',
        technologyUsed: ['Wordpress', 'PHP', 'mySQL', 'Campaign Monitor'],
        image: zinkAndSons,
        URL: ''
      },
      {
        clientName: 'Newtown Art Supplies',
        description:
          "Newtown Art Supplies is one of Australia's leading art supply stores selling accessories both online and from our art store just south of Sydney. Newtown is an inner city suburb of Sydney renowned for its off-beat and artistic culture.",
        year: '2016',
        technologyUsed: ['BigCommerce', 'Campaign Monitor', 'Bootstrap'],
        image: newtownAS,
        URL: ''
      }
    ]
  };

  onClickModalToggle = () => {
    this.setState({
      modalOpened: !this.state.modalOpened
    });
    if (this.state.name !== '') {
      this.setState({
        name: '',
        description: '',
        image: '',
        year: ''
      });
    }
  };

  onClickPortfolioInfo = e => {
    this.onClickModalToggle();

    this.setState(
      {
        name: e.target
          .querySelector('.portfolio-detail')
          .querySelector('.client-name').innerHTML,
        image: e.target.querySelector('img').src,
        description: e.target
          .querySelector('.portfolio-detail')
          .querySelector('.client-description').innerHTML,
        year: e.target
          .querySelector('.portfolio-detail')
          .querySelector('.client-year').innerHTML,
        technologies: e.target
          .querySelector('.portfolio-detail')
          .querySelector('.technology-used')
          .innerHTML.split(',')
      },
      () => {
        console.log(this.state.technologies);
      }
    );
  };

  render() {
    const portfolios = this.state.portfolios.map((portfolio, index) => {
      return (
        <div
          className="portfolio"
          onClick={this.onClickPortfolioInfo}
          key={index}
        >
          <img src={portfolio.image} alt={portfolio.clientName} />
          <div className="portfolio-detail">
            <p className="client-name">{portfolio.clientName}</p>
            <p className="client-description">{portfolio.description}</p>
            <p className="client-year">{portfolio.year}</p>
            <p className="technology-used">
              {portfolio.technologyUsed.map(technology => {
                return `${technology},`;
              })}
            </p>
          </div>
        </div>
      );
    });
    return (
      <React.Fragment>
        <section className="portfolio-section" id="portfolio">
          <div className="container">
            <h1 className="text-center">
              "Oi DeftByte <br /> where's the portfolio ?"
            </h1>
            <h2 className="text-center">
              Good question! Here are several works we have done in the past{' '}
              <br />
              (and probably present) with our <i>awesome</i> clients
            </h2>
          </div>
          <div className="flex-parent-wrap all-center portfolios">
            {portfolios}
          </div>
        </section>
        <TransitionGroup>
          {this.state.modalOpened ? (
            <CSSTransition
              key={this.state.image}
              in={true}
              appear={true}
              classNames={{
                enter: 'animated',
                enterActive: 'fadeIn',
                exit: 'animated',
                exitActive: 'fadeOut'
              }}
              timeout={500}
            >
              <div
                className="modal-portfolio client vh-fix"
                onClick={this.onClickModalToggle}
              >
                <div className="image animated fadeInLeft">
                  <div
                    className="image-bg"
                    style={{ backgroundImage: `url('${this.state.image}')` }}
                  />
                  {<img src={this.state.image} alt="" /> || (
                    <img src={l3bUnpad} alt="" />
                  )}
                </div>
                <div className="details">
                  <div className="animated fadeInLeft name">
                    {this.state.name || 'L3BUNPAD'}
                  </div>
                  <div className="animated fadeInLeft year">
                    {this.state.year || '2016'}
                  </div>
                  <div className="animated fadeInLeft description">
                    {this.state.description ||
                      'A curated marketplace that helps brands to connect to GetCraft’s network of creators across Journalism, Design, Photography, Video and Sponsored Channels such as Publishers (Digital online publishers) Influencers (Social Media, Celebrities, Experts). Projects are managed end-to-end through GetCraft’s platform and backed up by in-house production/editorial experts to help optimise performance.'}
                  </div>
                  <div className="technologies">
                    {this.state.technologies.map((technology, index) => {
                      return technology ? (
                        <p className="animated fadeIn" key={index}>
                          {technology}
                        </p>
                      ) : null;
                    })}
                  </div>
                </div>
              </div>
            </CSSTransition>
          ) : null}
        </TransitionGroup>
      </React.Fragment>
    );
  }
}

export default Portfolio;
