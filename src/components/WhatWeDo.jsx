import React from 'react';

import developmentIMG from '../assets/images/development.svg';
import seoIMG from '../assets/images/seo.svg';
import emailMarketingIMG from '../assets/images/email-marketing.svg';

const WhatWeDo = () => {
  return (
    <section className="what-we-do" id="about">
      <div className="container">
        <h1>What does deftbyte do ?</h1>
        <h2>
          We really do get your tech business idea comes into reality. How, you
          ask ? We sure do things such as
        </h2>
        <div className="flex-parent-wrap all-center single-wrapper">
          <div className="flex-parent-wrap all-center single">
            <img src={developmentIMG} alt="" />
            <p>Website, mobile and software development</p>
          </div>
          <div className="flex-parent-wrap all-center single">
            <img src={seoIMG} alt="" />
            <p>Search engine optimization</p>
          </div>
          <div className="flex-parent-wrap all-center single">
            <img src={emailMarketingIMG} alt="" />
            <p>Email marketing</p>
          </div>
        </div>
      </div>
    </section>
  );
};

export default WhatWeDo;
