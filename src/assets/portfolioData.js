const portfolios = [
  {
    clientName: 'Botler',
    description:
      'Botler is a chatbot that will help you arranging schedules. He will remind you and also tell you the time required to go to your destination. When you press botler notification, it will show the best route to take in google maps.',
    year: '2018',
    technologyUsed: [
      'React Native, Redux, Express.js, MongoDB, Mocha & Chai (TDD), Travis-CI'
    ],
    image: '',
    URL: ''
  },
  {
    clientName: 'Dinamika Oscar Agung',
    description:
      'PT. Dinamika Oscar Agung was in 2006 to provide solution for companies who demands well known engineering parts from Japan. With the principle of high level service, the company aims to satisfy its customer from the first sales process after sales process as a one stop solution for its clients.',
    year: '2018',
    technologyUsed: ['React.js, Node.js'],
    image: '',
    URL: ''
  },
  {
    clientName: 'GetCraft',
    description:
      'A curated marketplace that helps brands to connect to GetCraft’s network of creators across Journalism, Design, Photography, Video and Sponsored Channels such as Publishers (Digital online publishers) Influencers (Social Media, Celebrities, Experts). Projects are managed end-to-end through GetCraft’s platform and backed up by in-house production/editorial experts to help optimise performance. ',
    year: '2018',
    technologyUsed: ['React.js', 'Redux'],
    image: '',
    URL: ''
  },
  {
    clientName: 'Kembara Nusa',
    description:
      'Kembara Nusa adalah yayasan yang mengajak para dokter gigi dan teman-teman relawan lain dari berbagai latar belakang pendidikan yang ingin membantu untuk meningkatkan kesehatan gigi dan mulut di wilayah Indonesia, sekaligus mengenalkan kekayaan alam Indonesia di daerah yang kami pilih sebagai project kami.',
    year: '2018',
    technologyUsed: ['Vue.js', 'Node.js'],
    image: '',
    URL: ''
  },
  {
    clientName: 'Artistik Salindia Lima',
    description:
      'Artistik Salindia Lima is specialised in branding, design and marketing. Their services include graphic design, corporate identity, brand guidelines, packaging, print design and media',
    year: '2018',
    technologyUsed: ['Vue.js', 'Express.js', 'Bootstrap', 'Node.js', 'MongoDB'],
    image: '',
    URL: ''
  },
  {
    clientName: 'Gignest',
    description:
      'Gignest is an Indonesian talent showcase, providing people/ event organizer with selections of talents in the entertainment industry, including musician, stand-up comedian, magician, actor, sound engineer, and other relevant performers.',
    year: '2018',
    technologyUsed: ['Laravel', 'PHP', 'mySQL', 'SendGrid', 'Bootstrap'],
    image: '',
    URL: ''
  },
  {
    clientName: "Wahab Lukman's portfolio",
    description:
      "Latest updates on Wahab Lukman's activities involving around website and photography projects !",
    year: '2018',
    technologyUsed: ['Vue', 'Axios'],
    image: '',
    URL: ''
  },
  {
    clientName: 'Sartorial Bay',
    description:
      'Nowadays in Sydney, Massimo (the founder), continues his tailoring legacy through Sartorial Bay by offering his expertise to transform Australian men’s image. Massimo’s goal with Sartorial Bay is to blend heritage tailoring with the coastal lifestyle, to create a unique new style that embraces formal and casual wearing in an organic symbiosis.',
    year: '2017',
    technologyUsed: ['Wordpress'],
    image: '',
    URL: ''
  },
  {
    clientName: 'TunaKarya',
    description:
      'TunaKarya adalah suatu komunitas berbasis forum yang menjadikan wadah bagi para “tunakarya” untuk berdiskusi, bercerita, memberikan info apapun yang berhubungan dengan tujuan utama mereka yaitu untuk mendapatkan pekerjaan yang layak.',
    year: '2017',
    technologyUsed: ['Wordpress', 'mySQL', 'phpBB', 'Bootstrap'],
    image: '',
    URL: ''
  },
  {
    clientName: 'L3BUNPAD',
    description:
      'Lomba Lintas Lembah dan Bukit (L3B) merupakan acara rutin Fakultas Pertanian Universitas Padjadjaran yang pertama kali dilaksanakan tahun 1962 di kampus Unpad Dago dengan memanfaatkan lembah serta bukit di sekitarnya, diketuai oleh mahasiswa angkatan kedua di Fakultas Pertanian Universitas Padjadjaran (Faperta angkatan 1960) yaitu Ir. Sugiat dengan Kang Djungdjung Hickman (Faperta Unpad 1959) sebagai penanggung jawab dari Seksi Olah Raga Senat KMFP.',
    year: '2017',
    technologyUsed: ['HTML5', 'Vanilla JS', 'Bootstrap'],
    image: '',
    URL: ''
  },
  {
    clientName: 'Zink And Sons',
    description:
      'Zink and Sons has been dressing the gentlemen of Sydney since 1895 and have a reputation second to none, with Zink and Sons’ establishment now in its 6th generation under the ownership of Robert Jones and his son Daniel.',
    year: '2016',
    technologyUsed: ['Wordpress', 'PHP', 'mySQL', 'Campaign Monitor'],
    image: '',
    URL: ''
  },
  {
    clientName: 'Newtown Art Supplies',
    description:
      "Newtown Art Supplies is one of Australia's leading art supply stores selling accessories both online and from our art store just south of Sydney. Newtown is an inner city suburb of Sydney renowned for its off-beat and artistic culture.",
    year: '2016',
    technologyUsed: ['BigCommerce', 'Campaign Monitor', 'Bootstrap'],
    image: '',
    URL: ''
  },
  {
    clientName: 'Kadmium',
    description:
      "At Kadmium Art + Design supplies our aim is to combine specialty Art, Design and Architectural supplies with canvas stretching, in-house picture framing, imported stationery and unique gift ideas. Kadmium is much more than just your local art store, it's a hub for ideas, creativity and art classes as well as a platform for experiencing the latest art supplies technology.",
    year: '2016',
    technologyUsed: ['BigCommerce', 'Campaign Monitor', 'Bootstrap'],
    image: '',
    URL: ''
  }
];
